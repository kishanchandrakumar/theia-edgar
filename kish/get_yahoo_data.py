
from yahoofinancials import YahooFinancials
import pandas as pd
import datetime as dt

def get_yahoo_data(start_date, end_date, tickers):
    """
    A function which summarises stock price data and returns as 
    a single table, including returns calculated for the past 1, 
    2, 3, 5 and 10 business days. 
    
    This function requires you to run 
    > from yahoofinancials import YahooFinancials
    > import pandas as pd
    
    Arguments 
    *********
    start_date: pass in a string in the format YYYY-MM-DD
    end_date  : pass in a string in the format YYYY-MM-DD
    tickers   : pass in a string if a single ticker symbol requested,
                otherwise pass in a list of strings with ticker symbols
    """
    # convert to DateTime and take away 20 days
    start_date_dt = dt.datetime.strptime(start_date, '%Y-%m-%d')
    start_date_minus_20 = start_date_dt - dt.timedelta(20)
    
    #Convert back to string to pass into YahooFinancials:
    start_date_m20_str = dt.datetime.strftime(start_date_minus_20, '%Y-%m-%d')
    historical_prices = YahooFinancials(tickers)\
        .get_historical_price_data(start_date_m20_str, end_date, 'daily')
    
    # Initialise empty dataframe to concatenate to:
    cols = ['date', 'high', 'low', 'open', 'close', 'volume', 'adjclose', 'Symbol']
    df_out = pd.DataFrame(columns = cols)
    
    # Loop through tickers and concat to above dataframe
    for ticker in historical_prices.keys():
        prices    = historical_prices[ticker]['prices'] #Dictionary of data
        df_ticker = pd.DataFrame(prices)  #Converts price data to dataframe
        
        # Add column with ticker symbol for each row
        df_ticker['Symbol'] = ticker     
        
        # convert 'formatted_date' to datetime object
        df_ticker['formatted_date'] = pd.to_datetime(df_ticker['formatted_date'])
        df_ticker.set_index('formatted_date', inplace=True)

        # calculate and append 1, 2, 3, 5, and 10 day returns
        df_ticker['1daily_return']  = df_ticker['adjclose'].pct_change()
        df_ticker['2daily_return']  = (df_ticker['adjclose'] - df_ticker['adjclose'].shift(2))/df_ticker['adjclose'].shift(2)
        df_ticker['3daily_return']  = (df_ticker['adjclose'] - df_ticker['adjclose'].shift(3))/df_ticker['adjclose'].shift(3)
        df_ticker['5daily_return']  = (df_ticker['adjclose'] - df_ticker['adjclose'].shift(5))/df_ticker['adjclose'].shift(5)
        df_ticker['10daily_return'] = (df_ticker['adjclose'] - df_ticker['adjclose'].shift(10))/df_ticker['adjclose'].shift(10)
        
        # Finally appends this data onto df_out
        df_out    = df_out.append(df_ticker) 
        
        
    # Drop date, open and close price columns
    df_out.drop(columns=['date','open','close'], inplace=True)
    
    # rearrange column order to match requirements
    df_out = df_out[['high', 'low', 'adjclose', 'volume', '1daily_return',
       '2daily_return', '3daily_return', '5daily_return', '10daily_return', 'Symbol',]]
    
    # rename 'adjclose' to 'price'
    df_out.rename(columns={'adjclose':'price'}, inplace=True)
    
    # Remove unneeded rows dating before the user-specified start_date
    ind = df_out.loc[start_date_m20_str:start_date].index 
    df_out.drop(ind, inplace=True)
    
    return df_out